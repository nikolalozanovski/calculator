#include "cppunit/TestCase.h"
#include "cppunit/TestFixture.h"
#include "cppunit/ui/text/TextTestRunner.h"
#include "cppunit/extensions/HelperMacros.h"
#include "cppunit/extensions/TestFactoryRegistry.h"
#include "cppunit/TestResult.h"
#include "cppunit/TestResultCollector.h"
#include "cppunit/TestRunner.h"
#include "cppunit/BriefTestProgressListener.h"
#include "cppunit/CompilerOutputter.h"
#include "cppunit/XmlOutputter.h"
#include "cppunit/TestAssert.h"
#include "Source.h"

class unitTest :  public CPPUNIT_NS::TestFixture
{
public:
  void getParent();
  void setDecimalTrue();
  void setDecimalFalse();
  void formVectorSize();
  void checkboxState();
  CPPUNIT_TEST_SUITE(unitTest);
  CPPUNIT_TEST( setDecimalTrue );
   CPPUNIT_TEST( setDecimalFalse );
  CPPUNIT_TEST( getParent );
  CPPUNIT_TEST( formVectorSize );
  CPPUNIT_TEST( checkboxState );
  CPPUNIT_TEST_SUITE_END();
};
  CPPUNIT_TEST_SUITE_REGISTRATION( unitTest );
void unitTest::checkboxState()
{
  form f(CW_USEDEFAULT,CW_USEDEFAULT,GetSystemMetrics(SM_CXFULLSCREEN),GetSystemMetrics(SM_CYFULLSCREEN));
  Checkbox ch(f,1,1,1,1);
  CPPUNIT_ASSERT_EQUAL(ch.getState(), false);
}
void unitTest::formVectorSize()
{
  form f(CW_USEDEFAULT,CW_USEDEFAULT,GetSystemMetrics(SM_CXFULLSCREEN),GetSystemMetrics(SM_CYFULLSCREEN));
  form f1(CW_USEDEFAULT,CW_USEDEFAULT,GetSystemMetrics(SM_CXFULLSCREEN),GetSystemMetrics(SM_CYFULLSCREEN));
  form f2(CW_USEDEFAULT,CW_USEDEFAULT,GetSystemMetrics(SM_CXFULLSCREEN),GetSystemMetrics(SM_CYFULLSCREEN));
  CPPUNIT_ASSERT_EQUAL(f2.size(), 3);
}
void unitTest::getParent()
{
  form f(CW_USEDEFAULT,CW_USEDEFAULT,GetSystemMetrics(SM_CXFULLSCREEN),GetSystemMetrics(SM_CYFULLSCREEN));
  Formbutton fb(f,1,1,1,1);
  CPPUNIT_ASSERT_EQUAL(fb.GetRealParent(fb.buttonHandle), f.mainWindowHandle);
}

//this function is used to test if number is converted into decimal
//by adding '0' za first element
void unitTest::setDecimalTrue()
{
  std::string decimal{".0520"};
  std::string insertion=toDec(decimal);
  CPPUNIT_ASSERT_EQUAL(std::string("0.0520"), insertion);
}
//this function is used to test if '0' is not added to element
void unitTest::setDecimalFalse()
{
 std::string decimal{"5.0520"};
  std::string insertion=toDec(decimal);
  CPPUNIT_ASSERT_EQUAL(std::string("5.0520"), insertion);
}
int main()
{
CPPUNIT_NS::TestResult controller;

//--- Add a listener that colllects test result
CPPUNIT_NS::TestResultCollector Tresult;
controller.addListener( &Tresult );        

//--- Add a listener that print dots as test run.
CPPUNIT_NS::BriefTestProgressListener progress;
controller.addListener( &progress );      

//--- Add the top suite to the test runner
CPPUNIT_NS::TestRunner runner;
runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
runner.run( controller );

return Tresult.wasSuccessful() ? 0 : 1;
}
