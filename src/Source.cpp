#include "Source.h"


//declaration of main form(window)
 form myform(CW_USEDEFAULT,CW_USEDEFAULT,GetSystemMetrics(SM_CXFULLSCREEN),GetSystemMetrics(SM_CYFULLSCREEN));

//declaration of two textboxes for myform window and labels
//label for result textbox
// Label lResult(myform,helper::getXRes()/3,helper::getYRes()/10-15,"Result is : ");
//textbox for printing result
 textbox result(myform,helper::getXRes()/3,helper::getYRes()/10,400,20);
//textbox for printing numbers from button press
 textbox screen(myform,helper::getXRes()/3,helper::getYRes()/7,400,20);
//label for current number textbox
// Label lNumber(myform,helper::getXRes()/3,helper::getYRes()/7-15,"Current number is : ");

//declaration of buttons for numbers
  Formbutton zero(myform,helper::getXRes()/3+175,helper::getYRes()/3+225,50,50);
  Formbutton one(myform,helper::getXRes()/3,helper::getYRes()/3,50,50);
  Formbutton two(myform,helper::getXRes()/3+175,helper::getYRes()/3,50,50);
  Formbutton three(myform,helper::getXRes()/3+350,helper::getYRes()/3,50,50);
  Formbutton four(myform,helper::getXRes()/3,helper::getYRes()/3+75, 50,50);
  Formbutton five(myform,helper::getXRes()/3+175,helper::getYRes()/3+75,50,50);
  Formbutton six(myform,helper::getXRes()/3+350,helper::getYRes()/3+75,50,50);
  Formbutton seven(myform,helper::getXRes()/3,helper::getYRes()/3+150,50,50);
  Formbutton eight(myform,helper::getXRes()/3+175,helper::getYRes()/3+150,50,50);
  Formbutton nine(myform,helper::getXRes()/3+350,helper::getYRes()/3+150,50,50);

//declaration of buttons for operations
  Formbutton add(myform,helper::getXRes()/3-125,helper::getYRes()/3+38,50,50);
  Formbutton substract(myform,helper::getXRes()/3-125,helper::getYRes()/3+113,50,50);
  Formbutton multiply(myform,helper::getXRes()/3+475,helper::getYRes()/3+38,50,50);
 Formbutton divide(myform,helper::getXRes()/3+475,helper::getYRes()/3+113,50,50);

//declaration of buttons for character removal
 Formbutton removed(myform,helper::getXRes()/3,helper::getYRes()/3+225,50,50);
 Formbutton deleted(myform,helper::getXRes()/3+350,helper::getYRes()/3+225,50,50);

//declaration of button to complete operation
 Formbutton equals(myform,helper::getXRes()/3,helper::getYRes()/3+300,400,50);

//declaration of button to insert dot
 Formbutton dot(myform,helper::getXRes()/3,helper::getYRes()/3-75,400,50);




helper help;


//enum declaration
operation op;

bool hasDot=false;
double tempFirst;
double tempResult;
/*
 * This function is used to add text to
 * the textbox name 'screen' (bottom one)
 */
void addText( std::string in)
{
  std::string temp{screen.getText()};
  temp+=in;
  screen.setWindowText(temp);
}
//function which inserts character '0' if dot '.' is first character
std::string toDec(std::string& str)
{
  if(str.front()!='.')
  {
    return str;
  }
  str.insert(0,"0");
  return str;
}
/*
 * This function calculates data based
 * on symbol given.
 */
double calculate()
{
  auto getScreen=screen.getText();
  if(getScreen.front()=='.')
    {
      //insert '0' to string
      getScreen=toDec(getScreen);
    }
  if(op==operation::Sadd)
    {
      //add
      return tempResult=tempFirst+std::stod(getScreen),tempResult;
    }
  else if(op==operation::Ssubstract)
    {
      //substract
      return tempResult=tempFirst-std::stod(getScreen),tempResult;
    }
  else if(op==operation::Smultiply)
    {
      //multiply
      return tempResult=tempFirst*std::stod(getScreen),tempResult;
    }
  else if(op==operation::Sdivide)
    {
      //if current number is, notify user and return the resultscreen number
      if(getScreen=="0")
	{
	help.CMessageBox("Division by zero not allowed!");
	return std::stod(result.getText());
	}
      //divide
      return tempResult=tempFirst/std::stod(getScreen),tempResult;
    }
  else
    {
      //log if no symbol is given
      help.log("calculation symbol error");
    }
  return std::stod(result.getText());
}




/*
* These functions are used as
* events which sets text to buttons
*/
void clickZero()
{
  addText("0");
}
void clickOne()
{
  addText("1");
}
void clickTwo()
{
   addText("2");
}
void clickThree()
{
  addText("3");
}
void clickFour()
{
  addText("4");
}
void clickFive()
{
  addText("5");
}
void clickSix()
{
  addText("6");
}
void clickSeven()
{
  addText("7");
}
void clickEight()
{
  addText("8");
}
void clickNine()
{
  addText("9");
}
/*
*This function is used when the button
*for addition is clicked
*/
void clickAdd()
{
  //cached values from textboxes
  auto resultScreen=result.getText();
  auto getScreen=screen.getText();
  //if bottom text box is empty
  if(getScreen=="")
    {
      //if top textbox is not empty
      if(resultScreen!="")
	{
	  //then set the value to temporary variable 
	  tempFirst=std::stod(resultScreen);
	}
    }
  else
    {
      //if bottom textbot has dot for first charater
      if(getScreen.front()=='.')
	{
	  //then set '0' as first
	  getScreen=toDec(getScreen);
	}
      //data is converted to double from string
       tempFirst= std::stod(getScreen);
      //update textboxes text
      result.setWindowText(std::to_string(tempFirst));
      screen.setWindowText("");
      //dot is removed from bottom textbox
      hasDot=false;
    }
  //settings the operation to addition
  op=operation::Sadd;
}
/*
* This function is used when the button
* for substraction is clicked.
* This function works identical to addition and multiple
* except symbol for operation is set substract
*/
void clickSubstract()
{
  auto resultScreen=result.getText();
  auto getScreen=screen.getText();
  if(getScreen=="")
    {
      if(resultScreen!="")
	{
	  tempFirst=std::stod(resultScreen);
	}
    }
  else
    {
      if(getScreen.front()=='.')
	{
	  getScreen=toDec(getScreen);
	}
      tempFirst= std::stod(getScreen);
      result.setWindowText(std::to_string(tempFirst));
      screen.setWindowText("");
   
      hasDot=false;
    }
  //settings the operation to substraction
  op=operation::Ssubstract;
}
/*
* This function is used when the button
* for multiply is clicked.
* This function works identical to addition and substraction
* except symbol for operation is set multiply
*/
void clickMultiply()
{
  auto resultScreen=result.getText();
  auto getScreen=screen.getText();
  if(getScreen=="")
    {
      if(resultScreen!="")
	{
	  tempFirst=std::stod(resultScreen);
	}
      
    }
  else
    {
      if(getScreen.front()=='.')
	{
	  getScreen=toDec(getScreen);
	}
      tempFirst= std::stod(getScreen);
      result.setWindowText(std::to_string(tempFirst));
      screen.setWindowText("");
      hasDot=false;
    }
  //settings the operation to multiply
  op=operation::Smultiply;
}
/*
* This function is used when the button
* for substraction is clicked.
* One difference between then other function
* is if number is divided by zero, result is
* set to 0
*/
void clickDivide()
{
  //cached values from textboxes
  auto resultScreen=result.getText();
  auto getScreen=screen.getText();

  //if current number is '0', get out!
  
  //if bottom textbox is empty or it has zero
  if(getScreen=="")
    {
      //if top textbox is not empty
      if(resultScreen!="")
	{
	  //then add the value as double
	  tempFirst=std::stod(resultScreen);
	}
      //set bottom textbot empty
      screen.setWindowText("");
    }
  else
    {
      //if bottom textbox has dot '.' as first element
      if(getScreen.front()=='.')
	{
	  //then add '0' as first
	  getScreen=toDec(getScreen);
	}
      //add the values as double
      tempFirst = std::stod(getScreen);
      //update textboxes text
      result.setWindowText(std::to_string(tempFirst));
      screen.setWindowText("");
      //dot is removed from bottom textbox
      hasDot=false;
    }
  op=operation::Sdivide;
}
/*
* This function is used when the button
* for equals is clicked.
*/
void clickEquals()
{
  //cache the values from textboxes
  auto resultScreen=result.getText();
  auto getScreen=screen.getText();
  //bottom textbox is empty then exit function
  if(getScreen=="")
    {
      help.log("Screen is empty");
      return;
    }
  //if text top textbox is not empty
  if(resultScreen!="")
    {
      //add the values as double
      tempFirst=std::stod(resultScreen);
    }
  //calculate data (see above)
  calculate();
  //Update textboxes text
  result.setWindowText(std::to_string(tempResult));
  screen.setWindowText("");
  //dot is removed from bottom textbox
  hasDot=false;
}
/*
* This function is used when the button
* for delete is clicked.
* This function reset calculator to default state
*/
void clickDeleted()
{
  screen.setWindowText("");
  result.setWindowText("");
  hasDot=false;
  tempFirst=0;
  tempResult=0;
  op=operation::Sdefault;
  help.log("all data cleared");
}

/*
* This function is used when the button
* for dot '.' is clicked.
* This function insert dot '.' into textbox
*/
void clickDot()
{
  std::string temp;
  //cache text
  temp=screen.getText();
  //checking if there is not dot '.' in textbox
  if((temp.back()!='.' || temp.empty()==true)&&hasDot==false)
    {
      //There is dot in string
      hasDot=true;
      //add dot to string
      temp+=".";
      //update textbox text
      screen.setWindowText(temp);
    }
}
/*
* This function is used when the button
* for remove is clicked.
* This function remove last element inserted
*/
void clickRemoved()
{
  std::string temp;
  temp=screen.getText();
  //if textbox is empty then exit function
  if(temp.empty())
    {
      return;
    }
  else
    {
      //if dot is the last element, update variable
      if(temp.back()=='.')
	hasDot=false;

      //last element removed
      temp.pop_back();
      //update textbox text
      screen.setWindowText(temp);
    }
}

/*
 * This function groups text 
 * insertion function
*/
void buttonText()
{
  one.setWindowText("1");
  two.setWindowText("2");
  three.setWindowText("3");
  four.setWindowText("4");
  five.setWindowText("5");
  six.setWindowText("6");
  seven.setWindowText("7");
  eight.setWindowText("8");
  nine.setWindowText("9");
  add.setWindowText("+");
  substract.setWindowText("-");
  multiply.setWindowText("*");
  divide.setWindowText("/");
  equals.setWindowText("=");
  deleted.setWindowText("C");
  zero.setWindowText("0");
  dot.setWindowText(".");
  removed.setWindowText("R");
}
/*
 * This function groups click 
 * events insertion function
*/
void events()
{
  one.setEvent(&clickOne);
  two.setEvent(&clickTwo);
  three.setEvent(&clickThree);
  four.setEvent(&clickFour);
  five.setEvent(&clickFive);
  six.setEvent(&clickSix);
  seven.setEvent(&clickSeven);
  eight.setEvent(&clickEight);
  nine.setEvent(&clickNine);
  
  add.setEvent(&clickAdd);
  substract.setEvent(&clickSubstract);
  multiply.setEvent(&clickMultiply);
  divide.setEvent(&clickDivide);
  
  equals.setEvent(&clickEquals);
  deleted.setEvent(&clickDeleted);
  zero.setEvent(&clickZero);

  dot.setEvent(&clickDot);
  removed.setEvent(&clickRemoved);
}





int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
		   PSTR szCmdLine, int iCmdShow)
{
  //set text and events to buttons
  events();
  buttonText();
  
  //set textboxes to readonly
  result.setReadOnly();
  screen.setReadOnly();

  //enter message loop (start application)
  myform.updateForm();//enter myform's message loop

  return 0;
}

