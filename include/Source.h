//include files
#ifndef _windows_H
#define _windows_H
#include <windows.h>
#endif
#ifndef _Form_H
#define _Form_H
#include "Form.h"
#endif
#ifndef _FormButton_H
#define _FormButton_H
#include "FormButton.h"
#endif
#ifndef _helper_H
#define _helper_H
#include "helper.h"
#endif

#ifndef _Textbox_H
#define _Textbox_H
#include "Textbox.h"
#endif

#ifndef _Label_H
#define _Label_H
#include "Label.h"
#endif
#ifndef _Checkbox_H
#define _Checkbox_H
#include "Checkbox.h"
#endif
#ifndef _iostream_
#define _iostream_
#include <iostream>
#endif
#ifndef _string_
#define _string_
#include <string>
#endif

//documentation in implementation for all data
std::string toDec(std::string& str);
//enumeration for mathematical operations
enum operation
  {
    Sadd=0,
    Ssubstract=1,
    Smultiply=2,
    Sdivide=3,
    Sdefault
  };
//declaration of main form(window)
extern form myform;

//declaration of two textboxes for myform window and labels
//label for result textbox
//extern  Label lResult;
//textbox for printing result
extern  textbox result;
//textbox for printing numbers from button press
extern  textbox screen;
//label for current number textbox
//extern  Label lNumber;

//declaration of buttons for numbers
extern   Formbutton zero;
 extern  Formbutton one;
extern   Formbutton two;
extern   Formbutton three;
extern   Formbutton four;
extern   Formbutton five;
extern   Formbutton six;
extern   Formbutton seven;
extern   Formbutton eight;
extern   Formbutton nine;

//declaration of buttons for operations
extern   Formbutton add;
extern   Formbutton substract;
extern  Formbutton multiply;
extern  Formbutton divide;

//declaration of buttons for character removal
extern  Formbutton removed;
extern  Formbutton deleted;

//declaration of button to complete operation
extern  Formbutton equals;

//declaration of button to insert dot
extern  Formbutton dot;

