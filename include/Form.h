#ifndef _windows_H
#define _windows_H
#include "windows.h"
#endif
#ifndef _helper_H
#define _helper_H
#include "helper.h"
#endif

#ifndef _iostream_
#define _iostream_
#include <iostream>
#endif
#ifndef _vector_
#define _vector_
#include <vector>
#endif

class form 
{
 private:
  //position on X cordinate
  int xPos;
  //position on Y cordinate
  int yPos;
  //horizontal size of window (form)
  int xSize;
  //vectical size of window (form)
  int ySize;
  //declaration of vector that holds handles of active windows
  __declspec(dllexport)	static std::vector<HWND>ActiveForms;
  //function used to create form
  __declspec(dllexport)	HWND Createform(HWND handle, int LxPos = CW_USEDEFAULT, int LyPos = CW_USEDEFAULT,                                            				int LxSize = GetSystemMetrics(SM_CXFULLSCREEN), int LySize = GetSystemMetrics(SM_CYFULLSCREEN));
  //winapi variables
  //message variable used on window procudure
  MSG msg;
  //window class
  WNDCLASS wndclass;
 protected:
  //window procudure
  __declspec(dllexport)	static LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
 public:
  //declaration of function which return size of vector
  __declspec(dllexport)	int size();
  //form handle
  HWND mainWindowHandle;
  //default constructor
  // __declspec(dllexport)	form();
  //explicit constructor used to create form
  __declspec(dllexport)	form(int LxPos = CW_USEDEFAULT, int LyPos = CW_USEDEFAULT,int LxSize = GetSystemMetrics(SM_CXFULLSCREEN), 
			     int LySize = GetSystemMetrics(SM_CYFULLSCREEN));
  //constructor
  __declspec(dllexport)	~form();
  //declaration of function for message loop
  __declspec(dllexport)	WPARAM updateForm();
  //declaration of function which sets text to window
  __declspec(dllexport)	void setWindowText(std::string message);
  //disabling copy constructor
  __declspec(dllexport)	form(const form&  f)=delete;
};

