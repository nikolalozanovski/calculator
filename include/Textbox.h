#ifndef _windows_H
#define _windows_H
#include "windows.h"
#endif

#ifndef _helper_H
#define _helper_H
#include "helper.h"
#endif

#ifndef _String_
#define _helper_
#include <string>
#endif

#ifndef _Form_H
#define _Form_H
#include "Form.h"
#endif
#ifndef _iostream_
#define _iostream_
#include <iostream>
#endif
#ifndef _vector_
#define _vector_
#include <vector>
#endif
class textbox
{
private:
int xPos;
int yPos;
int xSize;
int ySize;
std::string Text;
HWND CreateTextbox(HWND& parent,HWND handle, int LxPos, int LyPos,int LxSize, int LySize );

protected:
public:
__declspec(dllexport) std::string getText();
__declspec(dllexport) void setReadOnly();
__declspec(dllexport) void setReadAndWrite();
__declspec(dllexport) void setWindowText(std::string message);
HWND mainTBHandle;
__declspec(dllexport) static std::vector<HWND> activeTextBox;
__declspec(dllexport) textbox();
__declspec(dllexport) ~textbox();
__declspec(dllexport) textbox(form& parent, int xPos , int yPos,int xSize , int ySize);
};
