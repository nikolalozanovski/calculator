#ifndef _windows_H
#define _windows_H
#include "windows.h"
#endif
#ifndef _Form_H
#define _Form_H
#include "Form.h"
#endif
#ifndef _helper_H
#define _helper_H
#include "helper.h"
#endif
#ifndef _iostream_
#define _iostream_
#include <iostream>
#endif
#ifndef _string_
#define _string_
#include <string>
#endif

#ifndef _vector_
#define _vector_
#include <vector>
#endif
class Label
{
 private:
  HDC Lhdc;
  HWND Hlabel;
  HMENU child;
  int xSize;
  int ySize;
  
  SIZE labelSize;

  __declspec(dllexport)  void draw(const form& form,const long xCordText,const long yCordText,const std::string text);
 protected:
 public:
  int xPos;
  int yPos;
  std::string textInLabel;
  __declspec(dllexport) static std::vector<Label*> labels;
  __declspec(dllexport)  Label();
  __declspec(dllexport)  Label(const form& form,const long xCordText,const long yCordText,const std::string text);
  __declspec(dllexport)  ~Label();
  __declspec(dllexport) Label(const Label& l);

};
