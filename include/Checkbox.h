#ifndef _windows_H
#define _windows_H
#include "windows.h"
#endif
#ifndef _helper_H
#define _helper_H
#include "helper.h"
#endif
#ifndef _iostream_
#define _iostream_
#include <iostream>
#endif
#ifndef _vector_
#define _vector_
#include <vector>
#endif
#ifndef _algorithm_
#define _algorithm_
#include <algorithm> 
#endif
#ifndef _Form_H
#define _Form_H
#include "Form.h"
#endif
class Checkbox
{
 private:
 protected:
  
 public:
  //handle
  HWND handleCheckBox;
  //position on X cordinate
  int xPos;
  //position on Y cordinate
  int yPos;
  //horizontal size
  int xSize;
  //vectical size
  int ySize;
  //menu handle
  HMENU menuCB;
  //enumuration used for state
  enum Options{Unchecked=0,Checked=1} options;
  //creation of checkbox
  __declspec(dllexport) void createCheckBox(const HWND& parent);
  //vector of pair of checkbox pointer to object and pair of function pointer based on checkbox state
  __declspec(dllexport)  static std::vector<std::pair< Checkbox*,typename std::pair < void*,void*> > >CheckboxList;
  //default constructor
  __declspec(dllexport)  Checkbox();
  //explicit constructor
  __declspec(dllexport)  Checkbox(form& parent,int xpos,int ypos,int xsize,int ysize);
  //destructor
  __declspec(dllexport) ~Checkbox();
  //copy constructor
  __declspec(dllexport) Checkbox(const Checkbox& cb);
  //function that find checkbox in vector
  __declspec(dllexport) HWND iterateVector(LPARAM wparam);
  //function which sets event based on state
  __declspec(dllexport) void setEvent( void (*func)(),Options opt);
  //function which executes event based on state
  __declspec(dllexport) void executeEvent(bool opt);
  //function which returs the state of checkbox
  __declspec(dllexport) bool getState();
};
