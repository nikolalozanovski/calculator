
#ifndef _windows_H
#define _windows_H
#include <windows.h>
#endif
#ifndef _algorithm_H
#define _algorithm_H
#include <algorithm> 
#endif
#ifndef _vector_
#define _vector_
#include <vector>
#endif
#ifndef _Form_H
#define _Form_H
#include "Form.h"
#endif

   

class Formbutton
{
private:
public:
	int xSize;
	int ySize;
	int xPos;
	int yPos;
	 HWND buttonHandle;
	 __declspec(dllexport)	Formbutton();
	 __declspec(dllexport)	~Formbutton();
	 __declspec(dllexport)	Formbutton(form& parent, int xpos, int ypos, int xsize, int ysize);
	 __declspec(dllexport)	HWND createButton(HWND button, HWND parent, int xpos, int ypos, int xsize, int ysize);
	 __declspec(dllexport)  void setWindowText(std::string message);
	 __declspec(dllexport)  void setEvent( void (*func)());
	 __declspec(dllexport)	static std::vector<std::pair<HWND,void*> > ActiveButtons;
	 __declspec(dllexport)	HWND iterateVector(LPARAM wparam);
	 __declspec(dllexport)	HWND GetRealParent(HWND hWnd);
	 __declspec(dllexport)	void executeEvent();
 protected:
};
