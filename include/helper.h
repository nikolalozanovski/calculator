

#ifndef _windows_H
#define _windows_H
#include <windows.h>
#endif
#ifndef _string_H
#define _string_H
#include <string>
#endif
#ifndef _type_traits_H
#define _type_traits_H
#include <type_traits>
#endif
#ifndef _utility_H
#define _utility_H
#include <utility>
#endif
#ifndef _sstream_H
#define _sstream_H
#include <sstream>
#endif


class helper
{
public:
  __declspec(dllexport)	helper();
  __declspec(dllexport)   static void getLastError();
  
  __declspec(dllexport)	HWND GetRealParent(HWND hWnd);
  __declspec(dllexport)	void setWindowText(HWND hwnd,std::string message);
  
  __declspec(dllexport) static int getXRes();
  __declspec(dllexport) static int getYRes();
  
  template <typename message>
    __declspec(dllexport)	message CMessageBox(const message m)
    {
      std::ostringstream o;
      o << m;
      MessageBoxA(NULL, o.str().c_str(), "info", MB_OK);
      return m;
    }
  __declspec(dllexport)	void log( std::string m);
  __declspec(dllexport)	void ErrorExit(LPTSTR lpszFunction);
};
