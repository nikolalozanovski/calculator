#commands
cc=x86_64-w64-mingw32-g++
curDir=$(pwd)
outfile=API.exe
outdir=OUT/
sourceDir=src
objdir=OUT/obj
OBJ_FILES = $(filter-out $(objdir)/Source.o, $(wildcard $(objdir)/*.o))
SRC_FILES=$(sourceDir)/Source.cpp
LIBDLL=libCPA++_dll.a
OUTDLL=CPA++.dll
Calculator.exe: src/Source.cpp  $(outdir)$(OUTDLL)
	mkdir OUT/obj -p

	x86_64-w64-mingw32-windres resource.rc -o res.o
	mv *.o $(objdir)

	$(cc)   -Iinclude -L./src/  -llibCPA++_dll  -static $(outdir)$(OUTDLL) $(sourceDir)/Source.cpp  -o $(outdir)$(outfile) -std=c++11 -mwindows -lgdi32  -lcomctl32 -D_GLIBCXX_USE_CXX11_ABI=0 -m64  

